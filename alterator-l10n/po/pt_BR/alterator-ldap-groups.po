# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Alterator\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-06-06 22:31+0400\n"
"PO-Revision-Date: 2008-12-02 22:29-0300\n"
"Last-Translator: Fernando Martini <fmartini@altlinux.com.br>\n"
"Language-Team: pt_BR <fmartini@altlinux.com.br>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Portuguese\n"
"X-Poedit-Country: BRAZIL\n"
"X-Poedit-SourceCharset: utf-8\n"
"X-Poedit-Basepath: pt_BR\n"

#: standard input:1
#, fuzzy
msgid "Ldap group accounts"
msgstr "Contas de usuário LDAP"

#: standard input:1
msgid "Please enter correct bind data for this base"
msgstr ""

#: standard input:2
#, fuzzy
msgid "root dn:"
msgstr "Interface ROOT"

#: standard input:3
#, fuzzy
msgid "root password:"
msgstr "Sem senha"

#: standard input:4
msgid "Bind"
msgstr ""

#: standard input:5
#, fuzzy
msgid "or"
msgstr "para"

#: standard input:6
#, fuzzy
msgid "Select other"
msgstr "Selecionar profile"

#: standard input:7
#, fuzzy
msgid "Select grouplist source"
msgstr "Selecionar disco para uso"

#: standard input:8
#, fuzzy
msgid "Current auth state"
msgstr "Atual Estado"

#: standard input:9
msgid "TCB on this server"
msgstr ""

#: standard input:10
#, fuzzy
msgid "LDAP base on this server"
msgstr "Servidor LDAP:"

#: standard input:11
#, fuzzy
msgid "Other LDAP server"
msgstr "Servidor LDAP:"

#: standard input:12
#, fuzzy
msgid "Remote host:"
msgstr "Endereço do Host:"

#: standard input:13
msgid "remote base:"
msgstr ""

#: standard input:14
#, fuzzy
msgid "Use selected source"
msgstr "Selecione o seu país:"

#: standard input:15
#, fuzzy
msgid "Return to list"
msgstr "Retornar a Visão"

#: standard input:16
#, fuzzy
msgid "Used local:"
msgstr "local"

#: standard input:17
msgid "tcb"
msgstr ""

#: standard input:18
msgid "on localhost"
msgstr ""

#: standard input:19
#, fuzzy
msgid "Used base:"
msgstr "Nova Base DN:"

#: standard input:20
#, fuzzy
msgid "on host"
msgstr "host desconhecido"

#: standard input:21
#, fuzzy
msgid "Source selector"
msgstr "Porta Fonte"

#: standard input:22
msgid "New group:"
msgstr "Novo grupo:"

#: standard input:23
msgid "Create"
msgstr "Criar"

#: standard input:24
#, fuzzy
msgid "Account"
msgstr "Conta:"

#: standard input:25
#, fuzzy
msgid "Group name:"
msgstr "Grupo:"

#: standard input:26
#, fuzzy
msgid "Users in group:"
msgstr "Grupos de usuários"

#: standard input:27
#, fuzzy
msgid "Users out of group:"
msgstr "Grupos de usuários"

#: standard input:28
#, fuzzy
msgid "E-mail"
msgstr "Email"

#: standard input:29
msgid "Remove"
msgstr "Remover"

#: standard input:30
msgid "New email:"
msgstr "Novo Email:"

#: standard input:31
msgid "Add"
msgstr "Adicionar"

#: standard input:32
#, fuzzy
msgid "Group mapping"
msgstr "Drop mail"

#: standard input:33
#, fuzzy
msgid "Map to system group:"
msgstr "Pular Grupos de Sistemas"

#: standard input:34
msgid "Map to Samba group:"
msgstr ""

#: standard input:35
#, fuzzy
msgid "Group settings successfully updated"
msgstr "Configurações do servidor atualizada com sucesso"

#: standard input:36
msgid "Save settings"
msgstr "Salvar configurações"

#: standard input:37
#, fuzzy
msgid "Delete group"
msgstr "Não é possível excluir o grupo"

#: type/ldap-group-name.scm:12
#, fuzzy
msgid "only latin letters, digits, dot, space and '_' allowed"
msgstr "apenas digitos em letras permitido '-' '.', e '_'"

#. Add first item
#: backend3/ldap-groups:313
#, fuzzy
msgid "Select system group:"
msgstr "Pular Grupos de Sistemas"

#, fuzzy
#~ msgid "Members:"
#~ msgstr "Dezembro"

#~ msgid "Name:"
#~ msgstr "Nome:"

#, fuzzy
#~ msgid "New member:"
#~ msgstr "Novo Usuário"

#~ msgid "Apply"
#~ msgstr "Aplicar"

#, fuzzy
#~ msgid "E-mail:"
#~ msgstr "Email"

#~ msgid "Save"
#~ msgstr "Salvar"

#~ msgid "Update settings"
#~ msgstr "Definições de actualização"
