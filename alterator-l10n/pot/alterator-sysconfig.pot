# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-09-11 17:00+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: standard input:1
msgid "Proxy settings"
msgstr ""

#: standard input:1 ui/sysconfig/proxy/index.scm:7
msgid "Proxy server:"
msgstr ""

#: standard input:2 ui/sysconfig/proxy/index.scm:9
msgid "Port:"
msgstr ""

#: standard input:3 ui/sysconfig/proxy/index.scm:14
msgid "Account:"
msgstr ""

#: standard input:4 ui/sysconfig/proxy/index.scm:17
msgid "Password:"
msgstr ""

#: standard input:5 ui/sysconfig/proxy/index.scm:22
msgid "No Proxy for:"
msgstr ""

#: standard input:6 ui/sysconfig/proxy/index.scm:32
msgid "Apply"
msgstr ""

#: standard input:7 ui/sysconfig/proxy/index.scm:33
msgid "Reset"
msgstr ""

#: ui/sysconfig/proxy/index.scm:24
msgid "(multiple values should be comma separated)"
msgstr ""

#: ui/sysconfig/base/index.scm:26
msgid "Help"
msgstr ""

#: ui/sysconfig/base/index.scm:27
msgid "Next"
msgstr ""

#: ui/sysconfig/base/index.scm:31
msgid "Select your language:"
msgstr ""

#: ui/sysconfig/base/index.scm:32
msgid "Please select keyboard switch type:"
msgstr ""

#: backend3/sysconfig-base:31
msgid "Alt+Shift key"
msgstr ""

#: backend3/sysconfig-base:32
msgid "CapsLock key"
msgstr ""

#: backend3/sysconfig-base:33
msgid "Control+Shift keys"
msgstr ""

#: backend3/sysconfig-base:34
msgid "Control key"
msgstr ""

#: backend3/sysconfig-base:35
msgid "Alt key"
msgstr ""

#: backend3/sysconfig-base:36
msgid "Control+Shift keys (UK,RU,EN)"
msgstr ""

#: backend3/sysconfig-base:37
msgid "Default"
msgstr ""

#: backend3/sysconfig-base:38
msgid "Without dead keys"
msgstr ""
