# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Andrey Cherepanov <skull@kde.ru>, 2008, 2014, 2015.
msgid ""
msgstr ""
"Project-Id-Version: ru\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-03-04 13:39+0300\n"
"PO-Revision-Date: 2015-02-04 16:42+0300\n"
"Last-Translator: Andrey Cherepanov <cas@altlinux.ru>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms:  nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: standard input:1
msgid "ISP traffic shaper"
msgstr "Управление полосой пропускания"

#: standard input:1 input:15
msgid "Database"
msgstr "База данных"

#: standard input:2 input:16
msgid "Configuration"
msgstr "Настройка"

#: standard input:3
msgid "Please, first"
msgstr "Пожалуйста, сначала"

#: standard input:4
msgid "configure"
msgstr "настройте"

#: standard input:5
msgid "IP:"
msgstr "IP:"

#: standard input:6
msgid "Rate:"
msgstr "Скорость:"

#: standard input:7
msgid "kibit"
msgstr "кбит"

#: standard input:8
msgid "Add/Modify"
msgstr "Добавить или изменить"

#: standard input:9
msgid "IP/Rate:"
msgstr "IP/Скорость:"

#: standard input:10
msgid "Search"
msgstr "Поиск"

#: standard input:11
msgid "IP address"
msgstr "IP-адрес"

#: standard input:12
msgid "Rate"
msgstr "Скорость"

#: standard input:13
msgid "Show:"
msgstr "Показывать:"

#: standard input:14 input:32 input:40 input:48
msgid "Delete"
msgstr "Удалить"

#: standard input:17
msgid "Enable shapercontrol"
msgstr "Включить службу shapercontrol"

#: standard input:18 input:50
msgid "Apply"
msgstr "Применить"

#: standard input:19
msgid "Any adjustments below is a reason to auto stop service."
msgstr "При любых изменениях ниже служба будет остановлена автоматически."

#: standard input:20
msgid ""
"In case of NAT existence speed limit has sense only for incoming traffic. "
"Outgoing speed limit for clients is uncontrolled. External interface should "
"be marked as 'disable'."
msgstr ""
"В случае NAT ограничение скорости имеет смысл только для входящего трафика."
"Скорость исходящего трафика для клиентов не контролируется. Внешний интерфейс "
"следует пометить как 'disable'."

#: standard input:21
msgid ""
"In case service is active all traffic flow is forbidden. Only clients have "
"corresponding entrance in DataBase can communicate."
msgstr ""
"Если служба включена, весь трафик запрещается. Только клиентыс "
"соответствующей записью в базе данных могут связываться."

#: standard input:22
msgid ""
"In case of NAT existence, traffic shaper sees all outgoing traffic as from "
"IP == external interface."
msgstr ""
"В случае NAT управление полосой пропускания рассматривает весь трафиккак "
"исходящий с IP внешнего интерфейса."

#: standard input:23
msgid "Interface connected to the upstream:"
msgstr "Интерфейс внешней сети:"

#: standard input:24
msgid "(control outgoing speed limit for local clients)"
msgstr "(управление лимитом скорости исходящего трафика локальных клиентов)"

#: standard input:25
msgid "Interface connected to your network:"
msgstr "Интерфейс внутренней сети:"

#: standard input:26
msgid "(control incoming speed limit for local clients)"
msgstr "(управление лимитом скорости входящего трафика локальных клиентов)"

#: standard input:27
msgid "Network list for classid calculation"
msgstr "Список сетей для вычисления classid"

#: standard input:28 input:34 input:42
msgid "Add network:"
msgstr "Добавить сеть:"

#: standard input:29 input:35 input:43
msgid "Add"
msgstr "Добавить"

#: standard input:30
msgid "Format is: X.X.X.X/YY"
msgstr "Формат: X.X.X.X/YY"

#: standard input:31
msgid ""
"This list should contain all networks whose clients are exposed to speed "
"limit control."
msgstr ""
"Этот список должен содержать все сети, клиенты которых подлежатконтролю "
"пропускной полосы."

#: standard input:33
msgid "Internal networks, whose traffic is transmitted without shaping"
msgstr "Внутренние сети, трафик из которых не ограничивается"

#: standard input:36 input:44
msgid "Format is: X.X.X.X/YY or X.X.X.X"
msgstr "Формат: X.X.X.X/YY или X.X.X.X"

#: standard input:37
msgid ""
"Each entry below has corresponding rule for external interface. Specified "
"clients from local networks do not have limit on outgoing traffic."
msgstr ""
"Каждая запись ниже содержит соответствующее правило для внешнего интерфейса."
"Указанные клиенты из локальных сетей не ограничиваются по полосе пропускания."

#: standard input:38
msgid ""
"Useless in case external interface is set to 'disable'. Outgoing traffic is "
"not exposed to speed limit."
msgstr ""
"Бесполезно в случае, когда внешний интерфейс установлен в 'disable'."
"Исходящий трафик не подлежит ограничению скорости."

#: standard input:39
msgid ""
"Warning: In case 'external interface' parameter is set to real interface, "
"here must be listed IP addresses of external interface. IPs of this server. "
msgstr ""
"Внимание: Если внешний интерфейс задан, то здесь должны быть перечислены "
"IP адреса внешнего интерфейса."

#: standard input:41
msgid "External networks, whose traffic is transmitted without shaping"
msgstr "Внешние сети, трафик из которых не ограничивается"

#: standard input:45
msgid ""
"Each entry below has an corresponding rule for internal iface. Traffic from "
"specified networks is not exposed to speed limit."
msgstr ""
"Каждая запись ниже содержит соответствующее правило для внутреннего "
"интерфейса.Трафик из указанных сетей не подлежит ограничению скорости."

#: standard input:46
msgid ""
"Warning: Traffic from this server is also question to speed limit. Add here "
"IP address of internal interface to turn off speed limit for traffic from "
"this server to local clients."
msgstr ""
"Внимание: трафик с этого сервера также подлежит ограничению скорости."
"Добавьте здесь IP-адрес внутреннего интерфейса, чтобы отключить ограничение "
"скорости с этого сервера к локальным клиентам."

#: standard input:47
msgid ""
"Warning: If you don't specify here IP address of local interface, then local "
"clients that are absent in database, will be unable to communicate with this "
"server at all."
msgstr ""
"Внимание: если здесь не указан IP-адрес локального интерфейса, то локальные "
"клиенты, которые отсутствуют в базе данных, не смогут взаимодействовать с "
"этим сервером вообще."

#: standard input:49
msgid "Ratio between bandwidth rates in rules and in the database:"
msgstr "Коэффициент отношения скоростей в правилах и в базе данных:"

#: backend3/shapercontrol:100 backend3/shapercontrol:136
msgid "Not defined (bad configuration)"
msgstr "Не определен (неверная настройка)"

#: backend3/shapercontrol:103
msgid "Disable (Recommended for NAT)"
msgstr "Отключено (рекомендуется для NAT)"

#: backend3/shapercontrol:107 backend3/shapercontrol:141
msgid "(absent in system)"
msgstr "(отсутствует в системе)"

#: backend3/shapercontrol:376
msgid "10 lines"
msgstr "10 строк"

#: backend3/shapercontrol:377
msgid "20 lines"
msgstr "20 строк"

#: backend3/shapercontrol:378
msgid "50 lines"
msgstr "50 строк"

#: backend3/shapercontrol:379
msgid "100 lines"
msgstr "100 строк"

#: backend3/shapercontrol:490
msgid "Lines %s-%s of %s"
msgstr "Строки %s-%s из %s"

#~ msgid "Warning: In case "
#~ msgstr "Предупреждение: в случае "
