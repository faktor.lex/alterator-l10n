# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Andrey Cherepanov <skull@kde.ru>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: ru\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-08-05 11:15+0400\n"
"PO-Revision-Date: 2008-09-01 18:01+0400\n"
"Last-Translator: Andrey Cherepanov <skull@kde.ru>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms:  nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: standard input:1 input:21 ui/update-kernel/update/index.scm:116
msgid "Update kernel"
msgstr "Обновить ядро"

#: standard input:1 ui/update-kernel/index.scm:97
msgid "Loaded kernel release:"
msgstr "Релиз загруженного ядра:"

#: standard input:2 ui/update-kernel/index.scm:100
msgid "Loaded kernel flavour:"
msgstr "Тип загруженного ядра (flavour):"

#: standard input:3 ui/update-kernel/index.scm:103
msgid "Loaded kernel version:"
msgstr "Версия загруженного ядра:"

#: standard input:4 ui/update-kernel/index.scm:121
msgid "Installed kernels:"
msgstr "Установленные ядра:"

#: standard input:5 ui/update-kernel/index.scm:131
msgid "Make the kernel bootable by default"
msgstr "Сделать ядро загружаемым по умолчанию"

#: standard input:6 input:12
msgid "Notes "
msgstr "Замечание:"

#: standard input:7 ui/update-kernel/index.scm:136
msgid ""
"To make the kernel bootable by default, select the desired version in the "
"list above"
msgstr ""
"Чтобы сделать ядро загружаемым по умолчанию, выберите желаемую версию"

#: standard input:8 ui/update-kernel/index.scm:137
msgid "and click the 'Make the kernel bootable by default' button."
msgstr "в списке выше и нажмите кнопку 'Сделать ядро загружаемым по умолчанию'."

#: standard input:9 ui/update-kernel/index.scm:138
msgid "Restart the computer to boot with the selected kernel."
msgstr "Перезагрузите компьютер, чтобы загрузится с выбранным ядром."

#: standard input:10 ui/update-kernel/index.scm:150
msgid "Remove kernel"
msgstr "Удалить ядро"

#: standard input:11 ui/update-kernel/index.scm:162
msgid "Update kernel..."
msgstr "Обновить ядро..."

#: standard input:13 ui/update-kernel/index.scm:173
msgid "To install modules or update the kernel, click 'Update kernel'"
msgstr ""
"Чтобы установить модули или обновить ядро, нажмите кнопку 'Обновить ядро'"

#: standard input:14 ui/update-kernel/index.scm:174
msgid "(You need the latest kernel to install modules)."
msgstr "(чтобы установить модули нужна последняя версия ядра)."

#: standard input:15 ui/update-kernel/index.scm:175
msgid ""
"This action will require updating the list of packages available in the "
"repository,"
msgstr "Это потребует обновления списка пакетов доступных в репозитории"

#: standard input:16 ui/update-kernel/index.scm:176
msgid ""
"and may take some time (depending on the speed of the Internet connection)."
msgstr "и может занять некоторое время (зависит от скорости интернета)."

#: standard input:17 ui/update-kernel/index.scm:110
msgid "Kernel bootable by default:"
msgstr "Ядро загружаемое по умолчанию:"

#: standard input:18 ui/update-kernel/index.scm:159
msgid "Installed modules"
msgstr "Установленные модули"

#: standard input:19 ui/update-kernel/index.scm:165
msgid "Remove module"
msgstr "Удалить модуль"

#: standard input:20 ui/update-kernel/update/index.scm:107
msgid "Available kernel:"
msgstr "Доступное ядро:"

#: standard input:22 ui/update-kernel/update/index.scm:118
msgid "Install modules"
msgstr "Установить модули"

#: standard input:23
msgid "back"
msgstr "назад"

#: standard input:24 ui/update-kernel/update/index.scm:125
msgid "Available modules"
msgstr "Доступные модули"

#: ui/update-kernel/ajax.scm:60 ui/update-kernel/index.scm:46
msgid "Do you really want to delete kernel "
msgstr "Вы действительно хотите удалить ядро "

#: ui/update-kernel/ajax.scm:82 ui/update-kernel/index.scm:68
msgid "Do you really want to delete these modules?"
msgstr "Вы действительно хотите удалить выбранные модули?"

#: ui/update-kernel/index.scm:14 ui/update-kernel/index.scm:42
#: ui/update-kernel/index.scm:65 ui/update-kernel/update/index.scm:17
#: ui/update-kernel/update/index.scm:66 ui/update-kernel/update/index.scm:85
msgid "Please wait..."
msgstr "Пожалуйста, подождите..."

#: ui/update-kernel/index.scm:135 ui/update-kernel/index.scm:172
msgid "Notes"
msgstr "Замечание:"

#: ui/update-kernel/update/ajax.scm:34 ui/update-kernel/update/index.scm:38
msgid "Latest kernel is already installed on your system."
msgstr "Последнее ядро уже установлено в вашей системе."

#: ui/update-kernel/update/ajax.scm:63 ui/update-kernel/update/index.scm:69
msgid "Do you really want to update kernel?"
msgstr "Вы действительно желаете обновить ядро?"

#: ui/update-kernel/update/ajax.scm:79 ui/update-kernel/update/index.scm:88
msgid "Do you really want to install modules?"
msgstr "Вы действительно желаете установить модули?"

#: ui/update-kernel/update/index.scm:139
msgid "<<  back"
msgstr "<<  назад"

#: backend3/update-kernel:40
msgid "Failed to install kernel."
msgstr "Не удалось установить ядро."

#: backend3/update-kernel:49
msgid ""
"Failed to install kernel. Perhaps 'noexec' mount option used for /tmp.\n"
"\t\t\t\tPlease do not try to boot with this kernel."
msgstr "Ошибка обновления ядра. Возможно, для /tmp используется параметр монтирования 'noexec'.\n"
"Пожалуйста, не пытайтесь загрузиться с этим ядром."

#: backend3/update-kernel:82
msgid "Kernel update completed."
msgstr "Обновление ядра завершено."

#: backend3/update-kernel:136
msgid "Failed to install modules."
msgstr "Не удалось установить модули."

# Настройка вручную
#: backend3/update-kernel:139
msgid "Modules installed."
msgstr "Модули установлены."

#: backend3/update-kernel:326
msgid ""
"Error when updating the list of packages available in the repository. The "
"list of available packages may be out of date."
msgstr ""
"Ошибка при обновлении списка пакетов, доступных в репозитории. Список "
"доступных пакетов может быть неактуален."

#~ msgid ""
#~ "To make the kernel bootable by default, select the desired version in "
#~ "the \n"
#~ "list above and click the \"Make the kernel bootable by default\" button.\n"
#~ "Restart the computer to boot with the selected kernel."
#~ msgstr ""
#~ "Чтобы сделать ядро загружаемым по умолчанию, выберете желаемую версию \n"
#~ "в списке выше и нажмите кнопку \"Сделать ядро загружаемым по умолчанию"
#~ "\".\n"
#~ "Перезагрузите компьютер что бы загрузится с выбранным ядром."

#~ msgid ""
#~ "To install modules or update the kernel, click \"Update kernel\" \n"
#~ "(You need the latest kernel to install modules).\n"
#~ "This action will require updating the list of packages available in the "
#~ "repository, \n"
#~ "and may take some time (depending on the speed of the Internet "
#~ "connection)."
#~ msgstr ""
#~ "Чтобы установить модули или обновить ядро, нажмите кнопку \"Обновить ядро"
#~ "\"\n"
#~ "(Чтобы установить модули нужна последняя версия ядра). Это потребует\n"
#~ "обновления списка пакетов доступных в репозитории и может занять\n"
#~ "некоторое время (зависит он скорости интернета)."
