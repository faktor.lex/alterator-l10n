# translation of alterator-ldap-users.po to
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Sergey Lebedev <lsv@altlinux.ru>, 2009.
# Olexandr Yatsenko <piktor@bigmir.net>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: alterator-ldap-users\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2011-04-01 15:53+0400\n"
"PO-Revision-Date: 2009-08-22 01:16+0300\n"
"Last-Translator: Olexandr Yatsenko <piktor@bigmir.net>\n"
"Language-Team: Ukrainian <kde-i18n-doc@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 0.3\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: standard input:1
msgid "Ldap user accounts"
msgstr "Облікові записи користувачів LDAP"

#: standard input:1
msgid "You must enter bind data for base"
msgstr ""

#: standard input:2 input:18
#, fuzzy
msgid "on host"
msgstr "Адреса веб-сайта"

#: standard input:3
#, fuzzy
msgid "root dn:"
msgstr "Кореневий пристрій:"

#: standard input:4
msgid "root pw:"
msgstr ""

#: standard input:5
msgid "Bind"
msgstr ""

#: standard input:6
#, fuzzy
msgid "or"
msgstr "для"

#: standard input:7
#, fuzzy
msgid "Select other"
msgstr "Обрати"

#: standard input:8
#, fuzzy
msgid "Select userlist source"
msgstr "Оберіть зовнішні інтерфейси:"

#: standard input:9
#, fuzzy
msgid "Current auth state"
msgstr "Поточна дата:"

#: standard input:10
msgid "/etc/passwd file on this server"
msgstr ""

#: standard input:11
#, fuzzy
msgid "LDAP base on this server"
msgstr "Сервер LDAP:"

#: standard input:12
#, fuzzy
msgid "Other LDAP server"
msgstr "Сервер LDAP:"

#: standard input:13
#, fuzzy
msgid "Remote host:"
msgstr "Адреса веб-сайта:"

#: standard input:14
msgid "remote base:"
msgstr ""

#: standard input:15
#, fuzzy
msgid "Use selected source"
msgstr "Будь-ласка, оберіть країну:"

#: standard input:16
#, fuzzy
msgid "Return to list"
msgstr "Повернутися до списку"

#: standard input:17
#, fuzzy
msgid "Used base:"
msgstr "Новий базовий DN:"

#: standard input:19
#, fuzzy
msgid "Source selector"
msgstr "Вилучити обране"

#: standard input:20
#, fuzzy
msgid "New account:"
msgstr "Обліковий запис:"

#: standard input:21
msgid "Create"
msgstr "Створити"

#: standard input:22
#, fuzzy
msgid "Filter:"
msgstr "Посада:"

#: standard input:23
#, fuzzy
msgid "System only"
msgstr "Системні журнали"

#: standard input:24
msgid "Peoples"
msgstr ""

#: standard input:25
msgid "UID From:"
msgstr ""

#: standard input:26
#, fuzzy
msgid "to"
msgstr "авто"

#: standard input:27
msgid "Select"
msgstr "Обрати"

#: standard input:28
msgid "Do not forget save data..."
msgstr ""

#: standard input:29
#, fuzzy
msgid "User settings updated"
msgstr "Налаштування сервера успішно оновлено"

#: standard input:30
#, fuzzy
msgid "Account"
msgstr "Обліковий запис:"

#: standard input:31
msgid "Name:"
msgstr "Системне ім’я:"

#: standard input:32
msgid "Last name:"
msgstr "Прізвище:"

#: standard input:33
msgid "First name:"
msgstr "Ім’я:"

#: standard input:34
msgid "Patronym:"
msgstr "По батькові:"

#: standard input:35
msgid "Home directory:"
msgstr "Домашня тека:"

#: standard input:36
msgid "Shell:"
msgstr "Оболонка:"

#: standard input:37
msgid "Password:"
msgstr "Пароль:"

#: standard input:38
msgid "Generate automatically"
msgstr "Створти автоматично"

#: standard input:39
msgid "(enter passphrase)"
msgstr "(уведіть пароль)"

#: standard input:40
msgid "(repeat passphrase)"
msgstr "(повторіть пароль)"

#: standard input:41
msgid "Generate"
msgstr "Генерувати"

#: standard input:42
msgid "Photo"
msgstr ""

#: standard input:43 input:62
msgid "Add"
msgstr "Додати"

#: standard input:44
#, fuzzy
msgid "Del"
msgstr "Вилучити"

#: standard input:45
#, fuzzy
msgid "Select new user photo:"
msgstr "Вилучити користувача"

#: standard input:46
msgid "Upload"
msgstr "Завантажити"

#: standard input:47
msgid "Cansel"
msgstr ""

#: standard input:48
#, fuzzy
msgid "Group membership"
msgstr "Склад групи"

#: standard input:49
msgid "Member of"
msgstr ""

#: standard input:50
#, fuzzy
msgid "Available groups"
msgstr "Доступні правила:"

#: standard input:51
msgid "Work"
msgstr "Робота"

#: standard input:52
msgid "Organization:"
msgstr "Організація:"

#: standard input:53
#, fuzzy
msgid "Organisation Unit:"
msgstr "Підрозділ організації (OU):"

#: standard input:54
msgid "Title:"
msgstr "Посада:"

#: standard input:55
msgid "Phone:"
msgstr "Телефон:"

#: standard input:56
msgid "Mobile phone:"
msgstr "Мобільний телефон:"

#: standard input:57
msgid "Department:"
msgstr ""

#: standard input:58
msgid "Address:"
msgstr "Адреса:"

#: standard input:59
#, fuzzy
msgid "E-mail"
msgstr "Электрона пошта"

#: standard input:60
msgid "Remove"
msgstr "Вилучити"

#: standard input:61
msgid "New email:"
msgstr "Новий email:"

#: standard input:63
msgid "Save settings"
msgstr "Зберегти налаштування"

#: standard input:64
msgid "Delete user"
msgstr "Вилучити користувача"

#: type/ldap-account-name.scm:12
#, fuzzy
msgid "only small latin letters, digits dot and '_' allowed"
msgstr "дозволені лише цифри, малі латинські літери та '_'"

#: backend3/ldap-users:683
msgid "Passwords mismatch"
msgstr "Паролі не співпадає"

#, fuzzy
#~ msgid "Available"
#~ msgstr "Доступні правила:"

#, fuzzy
#~ msgid "Export selected"
#~ msgstr "Для позначених:"

#, fuzzy
#~ msgid "Home dir"
#~ msgstr "Домашня тека:"

#~ msgid "Login"
#~ msgstr "Увійти"

#, fuzzy
#~ msgid "Migration tool"
#~ msgstr "Кінцевий час"

#, fuzzy
#~ msgid "Migration tools"
#~ msgstr "Кінцевий час"

#, fuzzy
#~ msgid "Password {hash}"
#~ msgstr "Пароль:"

#, fuzzy
#~ msgid "Select migration source"
#~ msgstr "Обрати дію:"

#, fuzzy
#~ msgid "Set"
#~ msgstr "Обрати"

#, fuzzy
#~ msgid "Shell"
#~ msgstr "Оболонка:"

#, fuzzy
#~ msgid "Upload file:"
#~ msgstr "Звантажити файл:"

#~ msgid "Additional info"
#~ msgstr "Додаткова інформація"

#~ msgid "Existing users"
#~ msgstr "Існуючі користувачі:"

#~ msgid "New user account"
#~ msgstr "Новий обліковий запис користувача"

#~ msgid "Save"
#~ msgstr "Зберегти"

#~ msgid "User account:"
#~ msgstr "Обліковий запис:"

#~ msgid "User's List:"
#~ msgstr "Список користувачів:"
