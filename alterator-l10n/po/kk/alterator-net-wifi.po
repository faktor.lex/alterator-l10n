# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: alterator-l10n\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2012-04-19 15:58+0400\n"
"PO-Revision-Date: 2013-03-19 20:40+0600\n"
"Last-Translator: Baurzhan Muftakhidinov <baurthefirst@gmail.com>\n"
"Language-Team: Kazakh <kk_KZ@googlegroups.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: kk\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 1.5.4\n"

#: standard input:1
msgid "Wifi interfaces"
msgstr "Wifi интерфейстері"

#: standard input:1 ui/net-wifi/index.scm:91
msgid "Interface:"
msgstr "Интерфейс:"

#: standard input:2 ui/net-wifi/index.scm:98
msgid "Reconfigure interface"
msgstr "Интерфейсті қайта баптау"

#: standard input:3 ui/net-wifi/index.scm:94
msgid "Status:"
msgstr "Қалып-күйі:"

#: standard input:4 ui/net-wifi/index.scm:100
msgid "Configured networks"
msgstr "Бапталған желілер"

#: standard input:5 ui/net-wifi/index.scm:111
msgid "Enable"
msgstr "Іске қосу"

#: standard input:6 ui/net-wifi/index.scm:113
msgid "Network name:"
msgstr "Желі атауы:"

#: standard input:7 ui/net-wifi/index.scm:116
msgid "Auth type:"
msgstr "Аутентификация түрі:"

#: standard input:8 ui/net-wifi/index.scm:119
msgid "Change password:"
msgstr "Парольді өзгерту:"

#: standard input:9 ui/net-wifi/index.scm:122
msgid "Encryption:"
msgstr "Шифрлеу:"

#: standard input:10 ui/net-wifi/index.scm:127
msgid "Apply"
msgstr "Іске асыру"

#: standard input:11 ui/net-wifi/index.scm:128
msgid "Reset"
msgstr "Тастау"

#: standard input:12 ui/net-wifi/index.scm:129
msgid "Delete"
msgstr "Өшіру"

#: standard input:13 ui/net-wifi/index.scm:132
msgid "Configure new network"
msgstr "Жаңа желіні баптау"

#: standard input:14 ui/net-wifi/index.scm:137
msgid "Networks found:"
msgstr "Табылған желілер:"

#: standard input:15 ui/net-wifi/index.scm:139
msgid "Rescan"
msgstr "Қайта іздеу"

#: standard input:16 ui/net-wifi/index.scm:140
msgid "Configure"
msgstr "Баптау"

#: ui/net-wifi/index.scm:136
msgid "Name"
msgstr "Аты"

#: ui/net-wifi/index.scm:136
msgid "signal"
msgstr "сигнал"

#: ui/net-wifi/index.scm:136
msgid "authentication"
msgstr "аутентификация"

#: ui/net-wifi/index.scm:142
msgid "OK"
msgstr "ОК"

#: backend3/net-wifi:39
msgid "connection is not configured"
msgstr "байланыс бапталмады"

#: backend3/net-wifi:67
msgid "connected to network %s(%s)\\n"
msgstr "желіге байланысқан: %s(%s)\\n"

#: backend3/net-wifi:70
msgid "scanning for available networks..."
msgstr "қолжетерлік желілерді іздеу..."

#: backend3/net-wifi:73
msgid "connecting to network..."
msgstr "желіге байланыс орнату..."

#: backend3/net-wifi:76
msgid "disconnected"
msgstr "байланыспаған"

#: backend3/net-wifi:174
msgid "No password"
msgstr "Пароль жоқ"

#: backend3/net-wifi:175
msgid "WEP Password"
msgstr "WEP паролі"

#: backend3/net-wifi:178
msgid "WPA Personal (WPA-PSK)"
msgstr "WPA Personal (WPA-PSK)"

#: backend3/net-wifi:179
msgid "WPA2 Personal (WPA-PSK)"
msgstr "WPA2 Personal (WPA-PSK)"

#: backend3/net-wifi:227
msgid "Current network"
msgstr "Ағымдағы желі"

#: backend3/net-wifi:340
msgid "Not a wireless interface"
msgstr "Сымсыз желі интерфейсі емес"

#: backend3/net-wifi:366
msgid "Other network"
msgstr "Басқа желі"

#: backend3/net-wifi:387
msgid "Password is too short"
msgstr "Пароль тым қысқа"

#: backend3/net-wifi:404
msgid "Unable to create network"
msgstr "Желіні жасау мүмкін емес"
