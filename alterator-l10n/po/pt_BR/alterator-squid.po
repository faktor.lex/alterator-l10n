# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Alterator\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2011-04-01 15:53+0400\n"
"PO-Revision-Date: 2009-09-14 20:12-0300\n"
"Last-Translator: Fernando Martini <fmartini@altlinux.org>\n"
"Language-Team: pt_BR <fmartini@altlinux.com.br>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Portuguese\n"
"X-Poedit-Country: BRAZIL\n"
"X-Poedit-SourceCharset: utf-8\n"
"X-Poedit-Basepath: pt_BR\n"

#: standard input:1
msgid "Proxy server"
msgstr "Servidor Proxy"

#: standard input:1 input:34
msgid "Internal networks"
msgstr "As redes internas"

#: standard input:2
msgid ""
"Requests originated from the networks listed below are forwarded to the "
"external network. All other requests are blocked."
msgstr ""

#: standard input:3 input:38 input:47
msgid "New"
msgstr "Novo"

#: standard input:4
#, fuzzy
msgid "IP-network:"
msgstr "Rede VPN:"

#: standard input:5
#, fuzzy
msgid "(IP address/network bits)"
msgstr "(Máscara de endereço IP)"

#: standard input:6 input:41 input:54
msgid "Comment:"
msgstr "Comentários:"

#: standard input:7 input:20 input:42 input:55
msgid "Apply"
msgstr "Aplicar"

#: standard input:8 input:43 input:56
msgid "Reset"
msgstr "Resetar"

#: standard input:9 input:44 input:57
msgid "Delete"
msgstr "Deletar"

#: standard input:10 input:58
msgid "Back"
msgstr "Voltar"

#: standard input:11
#, fuzzy
msgid "Module main section"
msgstr "Definições Principais"

#: standard input:12
msgid ""
"General proxy-server parameters management and service runtime status "
"control."
msgstr ""

#: standard input:13
msgid "Enable proxy-server service"
msgstr "Serviço de servidor proxy Ativar"

#: standard input:14
msgid "Select proxying mode:"
msgstr "Selecione o modo de proxy:"

#: standard input:15
msgid "Select authentication mode:"
msgstr "Selecione o modo de autenticação:"

#: standard input:16
msgid "Proxy port:"
msgstr "Porta proxy:"

#: standard input:17 input:49 input:51
msgid "(port number)"
msgstr "(número da porta)(apenas um número)"

#: standard input:18
#, fuzzy
msgid "Allowed networks..."
msgstr "Redes permitidas:"

#: standard input:19
#, fuzzy
msgid "Allowed protocols..."
msgstr "Protocolos Suportados"

#: standard input:21
#, fuzzy
msgid "Domain access policy"
msgstr "Acesso de domínio"

#: standard input:22
msgid ""
"For each of the user group listed below the access to a number of domians on "
"the external network can exclusively be granted or rewoked."
msgstr ""

#: standard input:23
msgid "Group:"
msgstr "Grupo:"

#: standard input:24
#, fuzzy
msgid "Group access policy:"
msgstr "Grupos de usuários"

#: standard input:25
#, fuzzy
msgid "Allow access"
msgstr "Acesso de domínio"

#: standard input:26
#, fuzzy
msgid "Deny access"
msgstr "Acesso de domínio"

#: standard input:27 input:39
msgid "Suffix list:"
msgstr "Lista de sufixo:"

#: standard input:28 input:40
msgid "(Space separated list of domain suffixes, each starting with a dot)"
msgstr ""
"(Lista separada por espaço de sufixos de domínio, cada partida com um ponto)"

#: standard input:29
msgid "Save"
msgstr "Salvar"

#: standard input:30 input:37
#, fuzzy
msgid "Domain list"
msgstr "Lista de domínio:"

#: standard input:31
msgid ""
"Domains defined below can be used in the group-domain access policy "
"configured at the upper section \"User group access policy\"."
msgstr ""

#: standard input:32
#, fuzzy
msgid "Navigation:"
msgstr "Organização:"

#: standard input:33
#, fuzzy
msgid "Main section"
msgstr "Definições Principais"

#: standard input:35 input:45
#, fuzzy
msgid "External ports"
msgstr "Portas TCP extras:"

#: standard input:36
#, fuzzy
msgid "User group access policy"
msgstr "Grupos de usuários"

#: standard input:46
msgid ""
"Requests for the services (ports) listed below are forwarded to the external "
"network. All other requests are blocked."
msgstr ""

#: standard input:48
msgid "From port:"
msgstr "Da porta:"

#: standard input:50
msgid "To port:"
msgstr "Para porta"

#: standard input:52
msgid "Connection method:"
msgstr "Método de conexão:"

#: standard input:53
#, fuzzy
msgid "Enable transparent redirection"
msgstr "Permitir a coleta de dados"

#: interfaces/guile/backend/squid.scm:83
#: interfaces/guile/backend/squid.scm:272
#, fuzzy
msgid "All users"
msgstr "Adicionar Usuário:"

#: interfaces/guile/backend/squid.scm:85
#, fuzzy
msgid "Authenticated users"
msgstr "Autentificação"

#: interfaces/guile/backend/squid.scm:182
msgid "Transparent"
msgstr "Transparente"

#: interfaces/guile/backend/squid.scm:183
msgid "Normal"
msgstr "Normal"

#: interfaces/guile/backend/squid.scm:185
msgid "No authentication"
msgstr "Sem autenticação"

#: interfaces/guile/backend/squid.scm:186
msgid "Kerberos"
msgstr "Kerberos"

#: interfaces/guile/backend/squid.scm:187
msgid "PAM"
msgstr "PAM"

#: interfaces/guile/backend/squid.scm:188
msgid "Kerberos+PAM"
msgstr "Kerberos+PAM"

#: interfaces/guile/backend/squid.scm:190
msgid "Proxy"
msgstr "Proxy"

#: interfaces/guile/backend/squid.scm:191
msgid "Passthrough"
msgstr "Repasse"

#: interfaces/guile/backend/squid.scm:274
#, fuzzy
msgid "Athenticated users"
msgstr "Autentificação"

#: interfaces/guile/type/domain-suffix-list.scm:14
msgid "a space separated list of domain suffixes each starting with a dot"
msgstr ""
"uma lista separada por espaço de domínio sufixos de cada partida com um ponto"

#: interfaces/guile/type/domain-suffix.scm:14
msgid "a domain suffix starting with a dot"
msgstr "um sufixo de domínio de partida com um ponto"

#: ui/squid/networks/ajax.scm:14 ui/squid/domains/ajax.scm:14
#: ui/squid/safe-ports/ajax.scm:14
msgid "&lt;new&gt;"
msgstr "&lt;novo&gt;"

#: backend3/squid-commit:67
#, fuzzy
msgid "Unable to reload the proxy server service"
msgstr "Serviço de servidor proxy Ativar"

#: backend3/squid-commit:76
#, fuzzy
msgid "Unable to turn proxy server service %s"
msgstr "Serviço de servidor proxy Ativar"

#: backend3/squid-commit:82
#, fuzzy
msgid "Unable to set proxy server service startup status"
msgstr "Serviço de servidor proxy Ativar"

#~ msgid "(IP address)"
#~ msgstr "(Endereço IP)"

#~ msgid "Access management"
#~ msgstr "O gerenciamento de acesso"

#~ msgid "Add"
#~ msgstr "Adicionar"

#~ msgid "Add domain"
#~ msgstr "Adicionar domínio"

#~ msgid "Address mask:"
#~ msgstr "Máscara Endereço:"

#~ msgid "Delete domain"
#~ msgstr "Deletar domínio"

#~ msgid "Deny selected domains"
#~ msgstr "Negar domínios selecionado "

#~ msgid "Destination domains"
#~ msgstr "Domínios Destino"

#~ msgid "Groups"
#~ msgstr "Grupos"

#~ msgid "IP-address:"
#~ msgstr "Endereço IP:"

#~ msgid "Manage domains"
#~ msgstr "Gerenciar domínios"

#~ msgid "Networks"
#~ msgstr "Redes"

#~ msgid "Ports"
#~ msgstr "Portas"

#~ msgid "Registered ports"
#~ msgstr "Portos Registados"

#~ msgid "in progress..."
#~ msgstr "em andamento ..."

#~ msgid "manually updated"
#~ msgstr "atualizado manualmente"

#~ msgid "never"
#~ msgstr "nunca"

#~ msgid "unknown"
#~ msgstr "desconhecido"
